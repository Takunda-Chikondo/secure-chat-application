package thread;

import model.SocketUserMap;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ServerThread extends Thread {

    private ArrayList<SocketUserMap> maps;
    private ServerSocket serverSocket;

    public ServerThread (ServerSocket serverSocket){
        try {
            maps = new ArrayList<>();
            this.serverSocket = serverSocket;
        } catch (Exception e){
            System.out.println("Error in ServerThread");
        }
    }

    public void run(){
        System.out.println("Waiting for clients...");
        while(true){
            try {
                Socket socket = serverSocket.accept();
                BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                SocketUserMap map = new SocketUserMap(socket, fromClient);
                map.setListener(new ClientMessageListener(map, this));
                map.getListener().start();
                maps.add(map);
            } catch (Exception e){
                try {
                    for (SocketUserMap m:maps) {
                        m.dispose();
                    }
                    serverSocket.close();
                    System.out.println("Error in ServerThread");
                } catch (Exception e2){}
            }
        }

    }

    protected ArrayList<SocketUserMap> getMaps(){
        return this.maps;
    }

}
