package thread;


import driver.AsymmetricCryptography;
import driver.VerifyMessage;

import java.io.*;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

/**
 * Created by Arjun on 4/5/2017.
 */
public class MessageReaderFromServer extends Thread {
    private BufferedReader bufferedReader;
    private Socket socket;
    private volatile boolean interrupted = false;
    private PrivateKey key;
    private String pathOfOtherUserPK;
    private String myUsername;
    InputStream inputStream=null;
    private String dirPath;
    private static long previousSequenceNumber;

    public MessageReaderFromServer(Socket socket, PrivateKey key, String myUsername){
        this.socket = socket; // client socket
        this.key=key;
        this.myUsername = myUsername;
    }

    private static Timestamp ConvertStringToTimeStamp(String string) throws ParseException {
        Timestamp timeStamp = new Timestamp((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(string)).getTime());
        return timeStamp;
    }

    private static String GetTimeStamp(String message){
        return message.substring(0, message.indexOf(","));
    }

    private static String GetSequenceNumber(String message){
        return message.substring(message.indexOf(",")+1,message.lastIndexOf(","));
    }

    private static Boolean ValidateTimeStamp(String message) throws ParseException {
        String receivedTimestamp = GetTimeStamp(message);

        Timestamp previousTimeStamp = ConvertStringToTimeStamp(receivedTimestamp);
        Timestamp currentTimeStamp = new Timestamp(System.currentTimeMillis());
        long differenceInMilliseconds = currentTimeStamp.getTime() - previousTimeStamp.getTime();

        if(differenceInMilliseconds < 5000){
            return true;
        }
        System.err.println("could not validate the timestamp of the message received : time difference "+ differenceInMilliseconds);
        return false;
    }

    private static Boolean ValidateSequenceNumber(String message) {
        String receivedSequenceNumber = GetSequenceNumber(message);

        long sequenceNumber = Long.parseLong(receivedSequenceNumber);
        if(sequenceNumber > previousSequenceNumber){
            return true;
        }
        System.err.println("Could not validate the sequence number of the message received");
        System.err.println("received sequence number " +sequenceNumber +" previous sequence number " + previousSequenceNumber);
        return false;
    }

    private static Boolean ValidateMessage(String message) throws ParseException {
        return ValidateSequenceNumber(message) && ValidateTimeStamp(message);
    }

    private static String GetMessageBody(String message){
        return message.substring(message.lastIndexOf(",")+1);
    }

    public String getPublicKeyPath(){
        return pathOfOtherUserPK;
    }

    private PrivateKey getMyPrivateKey(AsymmetricCryptography ac) throws Exception {
        return ac.getPrivate("KeyPair/" + myUsername +"/privateKey");
    }
    public void run(){
        while(!interrupted && !socket.isClosed()) {

            try {
                System.out.println("(Waiting for messages or Type in a message to send)");
                 inputStream = socket.getInputStream();
                this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream)); // gets messages from server

                String message = bufferedReader.readLine(); // reads the messages

                if (message == null) {
                     System.out.println("Server Went Offline, Please Connect Later!");
                     this.interrupt();
                     System.exit(0);

                }
                if (message.contains("@ReceivePK:")){
                    String fromUsername = message.substring(message.indexOf(' ') + 1, message.length()).trim();
                    dirPath = "ReceivedPK/"+fromUsername;
                    File directory =  new File(dirPath);
                    if(!directory.exists()){
                        directory.mkdirs();
                    }
                    pathOfOtherUserPK = dirPath +  "/publickey";
                    File file = new File(pathOfOtherUserPK);

                    FileOutputStream fos = new FileOutputStream(file);
                    byte[] bytes = new byte[1024];
                    int count;

                    while ((count = inputStream.read(bytes)) > 0) {
                        fos.write(bytes, 0, count);
                        if (count < 1024){
                            break;
                        }
                    }
                    fos.close();

                    // sending public key in return
                    PrintWriter p = new PrintWriter(socket.getOutputStream());
                    p.println("@receiveSecondPK");
                    p.flush();

                    file = new File("KeyPair/"+myUsername + "/publicKey");

                    FileInputStream fin = new FileInputStream(file);
                    bytes = new byte[1024];
                    while ((count = fin.read(bytes)) > 0) {
                        socket.getOutputStream().write(bytes, 0, count);
                        if (count < 1024){
                            break;
                        }
                    }
                    fin.close();
                } else if(message.contains("@ServerSendingSecondPK: ")){
                    String uname = message.substring(message.indexOf(": ") + 2, message.length());
                    System.out.println("User " + uname + " has sent back his public key to me. ");
                    dirPath = "ReceivedPK/"+ uname;
                    File directory =  new File(dirPath);
                    if(!directory.exists()){
                        directory.mkdirs();
                    }
                    pathOfOtherUserPK = dirPath +  "/publickey";
                    File file = new File(pathOfOtherUserPK);

                    FileOutputStream fos = new FileOutputStream(file);
                    byte[] bytes = new byte[1024];
                    int count;
                    while ((count = inputStream.read(bytes)) > 0) {
                        fos.write(bytes, 0, count);
                        if (count < 1024){
                            break;
                        }
                    }
                    fos.close();
                }else if (message.trim().contains("@Message")){
                    File test1 = new File("ReceivedMessage.txt");
                    FileOutputStream fos = new FileOutputStream(test1);

                    // receive the file

                    byte[] bytes1 = new byte[1024];
                    System.out.println("reached");

                    int count;

                    while ((count = inputStream.read(bytes1)) > 0) {
                        fos.write(bytes1, 0, count);
                        if (count < 1024){
                            break;
                        }
                        System.out.println("Count: " + count);

                    }
                    fos.close();

                    System.out.println("(Message Received!)");
                    AsymmetricCryptography ac = new AsymmetricCryptography();
                    PrivateKey privateKey = getMyPrivateKey(ac);
                    VerifyMessage verifiedMessage = new VerifyMessage("ReceivedMessage.txt", dirPath + "/publickey");
                    String decrypted_msg = ac.decryptText(verifiedMessage.getMessage(), privateKey);

                    if(ValidateMessage(decrypted_msg)){
                        System.out.println(GetMessageBody(decrypted_msg));
                    }
                }




            } catch (java.net.SocketException e1) {
                // e1.printStackTrace();
                this.interrupt(); // socket must have been closed and hence an interruption is needed
            } catch (Exception e2) {
                // e2.printStackTrace();
            }
        }
    }

    public void interrupt(){
        this.interrupted = true;
        try {
            bufferedReader.close();
        } catch (Exception e){

        }
    }
}
